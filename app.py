import os
import boto3
from flask import Flask, jsonify, request

app = Flask(__name__)
USERS_TABLE = os.environ['USERS_TABLE']
offline = bool(os.environ.get('IS_OFFLINE'))

if offline:
    client = boto3.client(
        'dynamodb',
        region_name='localhost',
        endpoint_url='http://localhost:8000'
    )
else:
    client = boto3.client('dynamodb')

@app.route("/")
def hello():
    return "Hello World!"

@app.route("/users/<string:user_id>")
def get_user(user_id):
    resp = client.get_item(
        TableName=USERS_TABLE,
        Key={
            'userId': { 'S': user_id }
        }
    )
    item = resp.get('Item')
    if not item:
        return jsonify({'error': 'User does not exist'}), 404
    return jsonify({
        'userId': item.get('userId').get('S'),
        'userToken': item.get('userToken').get('S'),
        'tokenExpiry': item.get('tokenExpiry').get('S')
    })

@app.route("/users", methods=["POST"])
def create_user():
    content = request.get_json()
    user_id = content['userId']
    user_token = content['userToken']
    token_expire = content['tokenExpiry']
    if not user_id: # or not name:
        return jsonify({'error': 'Please provide userId and name'}), 400
    resp = client.put_item(
        TableName=USERS_TABLE,
        Item={
            'userId': {'S': user_id },
            'userToken': {'S': user_token },
            'tokenExpiry': {'S': token_expire}
        }
    )

    return jsonify({
        'userId': user_id,
        'userToken': user_token,
        'tokenExpiry': token_expire
    })

@app.route("/token/<string:token_id>")
def validate_token(token_id):
    return("Your token ID:{} is going to return valid or invalid".format(token_id))

@app.route("/token/new", methods=["POST"])
def get_new_token():
    return("After validation of user identity is verified return users new token.")

@app.route("/token/refresh", methods=["POST"])
def refresh_token():
    return("After validation return refresh token")

@app.route("/login", methods=["GET", "POST"])
def login():
    return("Define login code including redirects and auth with salesforce.")

@app.route("/hours", methods=["POST"])
def get_user_hours():
    content = request.get_json()
    user_id = content["userId"]

@app.route("/hours", methods=["POST"])
def set_user_hours():
    return("Function to set user hours")